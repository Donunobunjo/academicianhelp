<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sale extends Model
{
    protected $fillable = [
        'film_id', 'user_id', 'filmTitle','unitPrice','quantity',
    ];
}
