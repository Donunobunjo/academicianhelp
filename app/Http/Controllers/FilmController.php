<?php

namespace App\Http\Controllers;

use App\Film;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator;

class FilmController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $films =Film::all();
        return Response::json(['films'=>$films]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        $validator = Validator::make($request->all(),[
            'title'=>'required',
            'genre'=>'required',
            'unitPrice'=>'required',
            'reOrderLevel'=>'required',
        ]);
        if ($validator->fails()){
            return Response::json(['errMsg'=>'Validation error']);
        }
        $film=Film::create([
            'title'=>$request->title,
            'genre'=>$request->genre,
            'unitPrice'=>$request->unitPrice,
            'reOrderLevel'=>$request->reOrderLevel
            
        ]);
        if($film){
            return Response::json(['message'=>'Film added successfully','film'=>$film]);
        }
        return Response::json(['message'=>'Film was not created']);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Film  $film
     * @return \Illuminate\Http\Response
     */
    public function show(Film $film)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Film  $film
     * @return \Illuminate\Http\Response
     */
    public function edit(Film $film)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Film  $film
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $film=Film::find($id);
        $film->update($request->all());
        if($film){
            return Response::json(['message'=>'Film updated successfully','newfim'=>$film]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Film  $film
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $film=Film::find($id);
        $film->delete();
        return Response::json(['message'=>'Film deleted successfully','film'=>$film]);
    }
}
