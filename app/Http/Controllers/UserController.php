<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator;
use App\User;

class UserController extends Controller
{
    public function register(Request $request){
        $validator = Validator::make($request->all(),[
            'name'=>'required',
            'email'=>'required|email',
            'password'=>'required',
            'isAdmin'=>'required',
            'address'=>'required',
            'dob'=>'required'
        ]);
        if ($validator->fails()){
            return Response::json(['errMsg'=>'Validation error']);
        }
        $user = User::create([
            'name'=>$request->name,
            'email'=>$request->email,
            'password'=>bcrypt($request->password),
            'isAdmin'=>$request->isAdmin,
            'address'=>$request->address,
            'dob'=>$request->dob
        ]);
        if ($user){
            $token = $user->createToken('myToken')->accessToken;
            $name = $user->name;
            $isAdmin = $user->isAdmin;
            return Response::json(['name'=>$name,'token'=>$token,'isAdmin'=>$isAdmin,'msg'=>'success']);
        }
        else{
            return Response::json(['errMsg'=>'Error ocurred','msg'=>'failure']);
        }
    }

    public function login(Request $request){
        
        $credentials=['email'=>$request->email,'password'=>$request->password];

        if (Auth::attempt($credentials)){
            $name=Auth::user()->name;
            $token=Auth::user()->createToken('myToken')->accessToken;
            $isAdmin=Auth::user()->isAdmin;
            return Response::json(['name'=>$name,'token'=>$token,'isAdmin'=>$isAdmin,'msg'=>'success'],200);
        }

        return Response::json(['msg'=>'failure'],401);
    }
}
