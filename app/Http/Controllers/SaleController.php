<?php

namespace App\Http\Controllers;

use App\Sale;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;

class SaleController extends Controller
{
   
    public function index()
    {
        // $sales = Sale::all();
        // return Response::json(['sales'=>$sales]);
    }

    
    public function create()
    {
        //
    }

    
    public function store(Request $request)
    {
        // $validator = Validator::make($request->all(),[
        //     'film_id'=>'required',
        //     'filmTitle'=>'required',
        //     'unitPrice'=>'required',
        //     'quantity'=>'required',
        // ]);
        // if ($validator->fails()){
        //     return Response::json(['errMsg'=>'Validation error']);
        // }
        $user_id = Auth::user()->id;
        $sale=Sale::create([
            'film_id'=>$request->film_id,
            // 'user_id'=>Auth::id(),
            'user_id'=>$user_id,
            'user_id'=>$request->user_id,
            'filmTitle'=>$request->filmTitle,
            'unitPrice'=>$request->unitPrice,
            'quantity'=>$request->quantity
            
        ]);
        if($sale){
            return Response::json(['message'=>'Sale added successfully','sale'=>$sale]);
        }
        return Response::json(['message'=>'Sale was not created']);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Sale  $sale
     * @return \Illuminate\Http\Response
     */
    public function show(Sale $sale)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Sale  $sale
     * @return \Illuminate\Http\Response
     */
    public function edit(Sale $sale)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Sale  $sale
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Sale $sale)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Sale  $sale
     * @return \Illuminate\Http\Response
     */
    public function destroy(Sale $sale)
    {
        //
    }
}
