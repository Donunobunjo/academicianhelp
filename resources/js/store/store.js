import Vue from 'vue'
import Vuex from 'vuex'
import Axios from 'axios'
import VueAxios from 'vue-axios'
import baseurl from '../components/baseURL'
Vue.use(Vuex)
Vue.use(VueAxios,Axios)
const debug = process.env.NODE_ENV !== 'production';
export default new Vuex.Store({
    state:{
        films:[],
        token: localStorage.getItem('token')||null,
        name : localStorage.getItem('name')||null,
        isAdmin : localStorage.getItem('isAdmin')||null,
       
    },
    getters:{

    },
    mutations:{
        LOAD_FILMS(state, payload){
            state.films=payload.data.films
        },
        ADD_FILM(state, payload){
             state.films.splice(0,0,payload.data.film)
        },
        // ADD_SALE(state, payload){
        //     state.films.splice(0,0,payload.data.film)
        // },
        LOGOUT(state){
            state.token = null
            state.name= null
            state.isAdmin=null
        },
        DELETE_FILM(state,payload){
            
            if (payload.data.message=='Film deleted successfully'){
                state.films.splice(state.films.indexOf(payload.data.film))
            }
        },
        REGISTER(state, payload){
            state.name=payload.data.name
            state.token=payload.data.token
            state.isAdmin=payload.data.isAdmin
           
        },
        LOGIN(state,payload){
            state.name=payload.data.name
            state.token=payload.data.token
            state.isAdmin=payload.data.isAdmin
        },
    },
    actions:{
        async getFilms({commit}){
            return commit('LOAD_FILMS', await Axios.get(baseurl+'/film'))
        },
        async addFilm({commit},payload){
            return commit('ADD_FILM', await Axios.post(baseurl+'/film',payload))
        },
        async deleteFilm({commit},payload){
            return commit('DELETE_FILM', await Axios.delete(baseurl+'/film/'+payload.id))
        },
        // async addSale({commit},payload){
        //     return commit('ADD_SALE', await Axios.post(baseurl+'/film',payload))
        // },
        register({commit},profile){
            return new Promise((resolve, reject) => {
                Axios.post(baseurl+'/register', profile)
                .then(resp => {
                  const token = resp.data.token
                  const name = resp.data.name
                  const isAdmin = resp.data.isAdmin
                  localStorage.setItem('token', token)
                  localStorage.setItem('name',name)
                  localStorage.setItem('isAdmin',isAdmin)
                  Axios.defaults.headers.common['Authorization'] = 'Bearer '+ token
                  commit('REGISTER', resp)
                  resolve(resp)
                })
                .catch(err => {
                  localStorage.removeItem('token')
                  localStorage.removeItem('name')
                  localStorage.removeItem('isAdmin')
                  reject(err)
                })
              })
        },
        login({commit},credentials){
            return new Promise((resolve, reject) => {
                Axios.post(baseurl+'/login', credentials)
                .then(resp => {
                  const token = resp.data.token
                  const name = resp.data.name
                  const isAdmin = resp.data.isAdmin
                  localStorage.setItem('token', token)
                  localStorage.setItem('name',name)
                  localStorage.setItem('isAdmin',isAdmin)
                  Axios.defaults.headers.common['Authorization'] = 'Bearer '+ token
                  commit('LOGIN', resp)
                  resolve(resp)
                })
                .catch(err => {
                  localStorage.removeItem('token')
                  localStorage.removeItem('name')
                  localStorage.removeItem('isAdmin')
                  reject(err)
                })
              })
        },
        logout({commit}){
            return new Promise((resolve, reject) => {
                commit('LOGOUT')
                localStorage.removeItem('token')
                localStorage.removeItem('name')
                localStorage.removeItem('isAdmin')
                delete Axios.defaults.headers.common['Authorization']
                resolve()
                reject()
              })
        }
    },
    strict: debug
});