require('./bootstrap');
window.Vue = require('vue')

import App from './App.vue';
import VueRouter from 'vue-router'
import VueAxios from 'vue-axios'
import axios from 'axios'
import {routes} from '../routes'
import store from './store/store'
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'

Vue.use(VueRouter);
Vue.use(VueAxios, axios);
Vue.use(ElementUI)

const router = new VueRouter({
    mode: 'history',
    routes: routes
})

const app = new Vue({
    el: '#app',
    router: router,
    store,
    render: h => h(App),
})