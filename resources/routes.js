import Home from './js/components/Home.vue'
import Login from './js/components/Login.vue'
import Register from './js/components/Register.vue'
import AdminLogin from './js/components/AdminLogin.vue'
import Sales from './js/components/Sales.vue'
import AdminDashboard from './js/components/AdminDashboard.vue'
export const routes = [
    {
        name: 'home',
        path: '/',
        component: Home
    },
    {
        name: 'login',
        path: '/login',
        component: Login
    },
    {
        name: 'register',
        path: '/register',
        component: Register
    },
    {
        name: 'admin',
        path: '/adminlogin',
        component: AdminLogin
    },
    {
        name: 'admindashboard',
        path: '/admindashboard',
        component:  AdminDashboard
    },
    {
        name: 'sales',
        path: '/sales',
        component: Sales
    },
]